<?php
require '/../../bootstrap.php';

use Symfony\Component\DependencyInjection\Container;
use MOWAFW\Core\Application;

function webservice(Container $container, Application $app)
{
    $firewall = $container->get('mowafw.firewall');
    
    
    /**
     * Simple get request with no paramaters
     */
    $app->get('/', function ()
    {
        print "I'm a simple get route with no get parameters";
    });
    
    /**
     * Simple get request with a required parameter
     */
    $app->get('/get1/:param', function ($param)
    {
        print "I'm a simple get route with a required parameter<br>";
        print "Parameter passed: $param";
    });
    
    /**
     * Simple get request with an optional parameter.
     * Optional parameters can accept default values
     * You can also use closures in order to pass other object to the anonymous function
     */
    $app->get('/get2/(:param)', function ($param = 'default') use($app)
    {
        print "I'm a simple get route with an optional parameter<br>";
        print "Parameter passed: $param";
    });
    
    /**
     * Simple post request with a required parameter and an optional parameter
     */
    $app->post('/post/:foo/(:bar)', function ($foo, $bar = 'bar')
    {
        print "I'm a simple post route with a required parameter and an optional parameter<br>";
        print "foo = $foo<br>";
        print "bar = $bar";
    });
    
    // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Firewall Examples
    //
    // Authenticatied access
    //
    
    /**
     * A public route that logs the user in
     *
     * @route /auth/login
     * @type POST
     * @visibility Public
     */
    $app->post('/auth/login', function () use($app)
    {
        $firewall = $app->modules->firewall;
        
        $request = $app->request();
        $login = $request->post('login');
        $password = $request->post('password');
        
        try {
            $firewall->getAuthenticationProvider()
                ->login($login, $password, true);
            $app->response()
                ->setStatus(200);
            print "User $login logged";
        } catch (\MOWAFW\Authentication\Exception\AuthenticationException $e) {
            $app->response()
                ->setStatus(400);
            print $e->getMessage();
        }
    });
    
    /**
     * A public route that logs the user out
     *
     * @route /auth/logout
     * @type GET
     * @visibility Public
     */
    $app->get('/auth/logout', function () use($app)
    {
        $firewall = $app->modules->firewall;
        
        $firewall->getAuthenticationProvider()
            ->logout(true);
        
        $response = $app->response();
        
        $response->setBody('User logged out succesfully!');
    });
    
    /**
     * A protected get request
     */
    $app->get('/private/', function ()
    {
        print "I'm private";
    })->protect();
    
    // START WEBSERVICE
    $app->run();
}
