<?php
// bootstrap.php

// Version Check
$min_version = "5.4.0";
if (version_compare(PHP_VERSION, $min_version) <= 0) {
    throw new \Exception("PHP Version is " . PHP_VERSION . " but $min_version or greater is needed!");
    exit;
}

// Autoloader
require_once "vendor/autoload.php";

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use MOWAFW\Core\Application;
use MOWAFW\Type\FileSystem\File;
use MOWAFW\Core\ModuleController\Loader;
use MOWAFW\Core\ModuleController\ModuleCompilerPass;

$configDir = realpath(__DIR__ . '/app/config/') . DIRECTORY_SEPARATOR;

$container = new ContainerBuilder();
$container->setParameter('basedir', __DIR__);


$moduleLoader = Loader::factory($container);
$moduleLoader->addModulesFile(new File($configDir . 'modules.yml', false))
             ->addConfigFile(new File($configDir . 'config.yml', false))
             ->addServicesFile(new File($configDir . 'services.yml', false))
             ->initializeModules()
             ->registerLoadedModules()
             ->registerConfigAndServices()
             ->compile();


//Start webservice defined in index.php
if (function_exists('webservice'))
	webservice($container, $container->get("mowafw.application"));