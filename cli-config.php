<?php
// cli-config.php
require_once "bootstrap.php";
$entityManager = $container->get('doctrine.entityManager');

$entityManager->getEventManager()->addEventSubscriber(
    new \Doctrine\DBAL\Event\Listeners\MysqlSessionInit('utf8', 'utf8_unicode_ci')
);

$entityManager->getConnection()->getDatabasePlatform()
              ->registerDoctrineTypeMapping('enum', 'string');

$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($entityManager)
));